#include <stdio.h>
#include <string.h>
#include <time.h>
#include <stdlib.h>
#include <stdbool.h>

//deklaracija funkcij
void vislice();
void print_beseda();

//globalne konstante
#define POSKUSI 6




int main() {
    printf("VISLICE\n");
    printf("Pravila:\n1) namig: zivali slovenskih gozdov\n2) vse crke so majhne in brez sumnikov\n3) po sestih napacnih poskusih ste OBESENI!!\n4) ce navedete znak, ki ni del slo abecede, se igra ustavi in ste avtomaticno obeseni\n");


	//rendom stevilo
	srand(time(NULL));
	int n = rand() % 10;


    //izbor besede
    char values[10][10] = {"jazbec", "medved", "srna", "mocerad", "ris", "jez", "sova", "volk", "lisica", "veverica" };
    char *beseda = values[n];


	//spremenljivke
    int dolzina_b = strlen(beseda);
	int napake = 0;
	char *body = malloc(POSKUSI+1);
	char *guessed = malloc(dolzina_b);
	char falseWord[POSKUSI];

	memset(body, ' ', POSKUSI+1);
	memset(guessed, '_', dolzina_b);
	char guess;
	bool found;
	char* win;


	setvbuf(stdin, NULL, _IONBF, 0);


	do {
		found = false;
		printf("\n");
		vislice(napake, body);
		printf("\n");


		printf("Napacne crke: ");
		if(napake == 0){
		printf("/\n");
		}
		for (int i = 0; i < napake; ++i){
			printf("%c", falseWord[i]);
		}


		printf("\n");
		print_beseda(guessed, dolzina_b);

		do {
			scanf(" %c",&guess);
		} while ( getchar() != '\n' );

		if(guess!='a' || guess!='b' || guess!='c' || guess!='d' || guess!='e' || guess!='f' || guess!='g' || guess!='h' || guess!='i' || guess!='j' || guess!='k' || guess!='l' || guess!='m' || guess!='n' || guess!='o' || guess!='p' || guess!='r' || guess!='s' || guess!='t' || guess!='u' || guess!='v' || guess!='z'){
			printf("\nNapacni znak, program ponovno zazeni in pricni od zacetka.\n");
			break;
		}


		for (int i = 0; i < dolzina_b; ++i){
			if(beseda[i] == guess) {
				found = true;
				guessed[i] = guess;
			}
		}
		if(!found) {
			falseWord[napake] = guess;
			napake += 1;
		}
		win = strchr(guessed, '_');

	}while(napake < POSKUSI && win != NULL);

	if(win == NULL) {
		printf("\n");
		print_beseda(guessed, dolzina_b);
		printf("\nBravo, besedo ste pravilno uganili :)\n");
	} else {
		printf("\n");
		vislice(napake, body);
		printf("\nBili ste obeseni, pravilna beseda je bila: %s\n", beseda);
	}

	free(body);
	free(beseda);
	free(guessed);
	return EXIT_SUCCESS;
	
}


//beseda
void print_beseda(char* guess, int dolzina_b) {
	printf("Iskana beseda: ");
	for (int i = 0; i < dolzina_b; ++i)
	{
		printf(" %c ", guess[i]);
	}
	printf("\n");
}





//ta del kode sem dobila iz interneta, saj sama nisem znala narisati mozicla
void vislice(int napake, char* body) { 
	printf("Stevilo napak:%d\n", napake);
	switch(napake) {
		case 6: body[6] = '\\'; break;
		case 5: body[5] = '/'; break;
		case 4: body[4] = '\\'; break;
		case 3: body[3] = '|'; break;
		case 2: body[2] = '/'; break;
		case 1: body[1] = ')', body[0] = '('; break;
		default: break;
	}

	printf(" _________\n"
	       "|         |\n"
	       "|        %c %c\n"
	       "|        %c%c%c\n"
	       "|        %c %c\n"
	       "|             \n"
	       "|             ", body[0], body[1], body[2],
	       body[3], body[4], body[5], body[6]);
}